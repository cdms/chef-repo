#
# Cookbook Name:: war-deployer
# Recipe:: default
#
# Copyright 2014, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

# todo : stop tomcat service
service "tomcat6" do
  action :stop
end

# todo : copy *.war files to webapp directory.
remote_file "deploy *.war" do
  path "#{node['tomcat']['webapp_dir']}/#{node['WAS']['file_path']}"
  source "#{node['WAS']['ftp_url']}#{node['WAS']['file_path']}"
end

service "tomcat6" do
  action [:enable, :restart]
end
