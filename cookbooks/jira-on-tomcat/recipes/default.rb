#
# Cookbook Name:: jira-on-tomcat
# Recipe:: default
#
# Copyright 2014, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'tomcat::default'
include_recipe 'mysql::server'

DB_SH= "cat << EOF | mysql -u root --password='#{node['mysql']['server_root_password']}'\n"+
"create database if not exists #{node['jira-on-tomcat']['db_name']} character set utf8;\n"+

"grant all privileges on #{node['jira-on-tomcat']['db_name']}.* to '#{node['jira-on-tomcat']['db_user_name']}'@'localhost'\n"+
"identified by '#{node['jira-on-tomcat']['db_user_password']}';\n"+

#"grant all privileges on #{node['jira-on-tomcat']['db_name']}.* to '#{node['jira-on-tomcat']['db_user_name']}'@'#{node['hostname']}'\n"+
#"identified by '#{node['jira-on-tomcat']['db_user_password']}';\n"+

#"grant all privileges on #{node['jira-on-tomcat']['db_name']}.* to '#{node['jira-on-tomcat']['db_user_name']}'@'#{node['jira-on-tomcat']['ipaddress']}'\n"+
#"identified by '#{node['jira-on-tomcat']['db_user_password']}';\n"+

"flush privileges;\n"+
"exit\n"+
"EOF\n"

unless FileTest.exists?("#{node['tomcat']['webapp_dir']}/jira")
  remote_file "jira-war" do
    path "#{Chef::Config[:file_cache_path]}/jira-war.tar.gz"
    source "http://www.atlassian.com/software/jira/downloads/binary/atlassian-jira-6.2-war.tar.gz"
  end
  
  execute "untar-jira" do
    cwd Chef::Config[:file_cache_path]
    command "tar zxvf jira-war.tar.gz"
  end

  remote_file "jira lib for tomcat" do
    path "#{Chef::Config[:file_cache_path]}/jira-lib.zip"
    source "http://www.atlassian.com/software/jira/downloads/binary/jira-jars-tomcat-distribution-6.2-tomcat-6x.zip"
  end
  
  package  "unzip" do
    action :install
  end

  execute "unzip-jira-lib" do
    cwd Chef::Config[:file_cache_path]
    command "unzip -n jira-lib.zip -d #{node['tomcat']['lib_dir']}"
  end

  remote_file "mysql-connector" do
    path "#{Chef::Config[:file_cache_path]}/mysql-connector.tar.gz"
    source "http://downloads.mysql.com/archives/mysql-connector-java-5.1/mysql-connector-java-5.1.6.tar.gz"
  end

  execute "untar-mysql-connector" do
    cwd Chef::Config[:file_cache_path]
    command "tar zxvf mysql-connector.tar.gz"
  end

  execute "install-mysql-connector" do
    command "cp #{Chef::Config[:file_cache_path]}/mysql-connector-java-5.1.6/mysql-connector-java-5.1.6-bin.jar #{node['tomcat']['lib_dir']}"
  end

  execute "database setting" do
    command "#{DB_SH}"
  end

  
  # jira_home 만들기.
  template "#{Chef::Config[:file_cache_path]}/atlassian-jira-6.2-war/edit-webapp/WEB-INF/classes/jira-application.properties" do
    source "jira-application.properties.erb"
  end
  
  directory "#{node['jira-on-tomcat']['jira_home_path']}" do
    owner "#{node['tomcat']['user']}"
    action :create
  end

   # war떨구기!
  execute "build war" do
    cwd "#{Chef::Config[:file_cache_path]}/atlassian-jira-6.2-war"
    command "./build.sh"
  end

  execute "copy war" do
    cwd "#{Chef::Config[:file_cache_path]}/atlassian-jira-6.2-war"
    command "cp dist-tomcat/atlassian-jira-6.2.war #{node['tomcat']['webapp_dir']}/jira.war"
  end
end

# tomcat restart
service "tomcat6" do
  action [:enable, :restart]
end  
