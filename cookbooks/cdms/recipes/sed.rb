# sed -i 's/ubuntu/`cat /etc/hostname`/g' /etc/hosts
# sed -i 's/#{str_before}/#{str_after}/g' #{replace_file_path}

execute 'replace string' do
  if !node['cdms']['sed']['str_before'].nil?
    command "sed -i 's/#{node['cdms']['sed']['str_before']}/#{node['cdms']['sed']['str_after']}/g' #{node['cdms']['sed']['replace_file_path']}" 
    not_if "grep #{node['cdms']['sed']['str_after']} #{node['cdms']['sed']['replace_file_path']}"
  end
end
