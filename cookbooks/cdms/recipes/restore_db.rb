# get dump file from ftp server and restore database.
# mysql need user/password/database name 

if !node['cdms']['database']['name'].nil?
  # get backuped sql file from ftp server
  # ftp_url : ftp://61.43.139.25/
  # file : teletalkvi.sql
  remote_file "download backuped sql file from #{node['cdms']['restore_db']['ftp_url']}" do
    path "#{Chef::Config[:file_cache_path]}/#{node['cdms']['database']['name']}.sql"
    source "#{node['cdms']['restore_db']['ftp_url']}#{node['cdms']['database']['name']}.sql"
  end

  # restore database
  # mysql -u dice -phellodice teletalkvi < teletalkvi.sql
  execute "restore database" do
    command "mysql -u #{node['cdms']['database']['user']} -p#{node['cdms']['database']['password']} #{node['cdms']['database']['name']} < #{Chef::Config[:file_cache_path]}/#{node['cdms']['database']['name']}.sql"
  end
end
