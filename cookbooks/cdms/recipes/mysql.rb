# initial setup for MySQL
# 1. Create User
# 2. Create DATABASE
# 3. Authorize User to access DATABASE outside

include_recipe "database::mysql"

mysql_connection_info = {
  :host => "localhost",
  :username => 'root',
  :password => node['mysql']['server_root_password']
}

mysql_database node['cdms']['database']['name'] do
  connection mysql_connection_info
  action :create
end

mysql_database_user node['cdms']['database']['user'] do
  connection mysql_connection_info
  password node['cdms']['database']['password']
  database_name node['cdms']['database']['name']
  host '%'
  action :grant
end
