# import jar file to tomcat library

node['cdms']['tomcat']['jar_list'].each do |jar|
  remote_file "start #{jar}" do
    path "#{node['tomcat']['lib_dir']}/#{jar}"
    source "#{node['cdms']['tomcat']['jar_ftp_url']}#{jar}"
    mode "#{node['cdms']['tomcat']['jar_mode']}"
  end
end
