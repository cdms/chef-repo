# setup vm /etc/host 127.0.1.1 ubuntu -> 127.0.1.1 vm-hostname

#execute "change host name" do
#  command "sed -i 's/ubuntu/#{node['name']}/g' /etc/hosts"
#  command "sed -i 's/ubuntu/`cat /etc/hostname`/g' /etc/hosts"
#  not_if "grep #{node['name']} /etc/hosts"
#end

bash "change hostname" do
  user "root"
  code <<-EOF
  HOSTNAME=`cat /etc/hostname`
  sed -i 's/ubuntu/'"$HOSTNAME"'/g' /etc/hosts
  EOF
end
