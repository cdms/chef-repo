# war deploy to tomcat

# stop tomcat6, while deploy war file
#service "tomcat6" do
#  action :stop
#end

# copy *.war files to chef Cache dirtory.
if !node['cdms']['tomcat']['name'].nil?
  # ftp_url : ftp://61.43.139.25/
  # file : jira-6.2-complete.war    
  remote_file "start deploy #{node['cdms']['tomcat']['name']}'s .war file" do
    path "#{Chef::Config[:file_cache_path]}/#{node['cdms']['tomcat']['file']}"
    source "#{node['cdms']['tomcat']['ftp_url']}#{node['cdms']['tomcat']['file']}"
  end

  # create directory that is needed by war app.
  if !node['cdms']['tomcat']['directory']['path'].nil?
    directory "#{node['cdms']['tomcat']['directory']['path']}" do
      owner "#{node['tomcat']['user']}"
      mode "#{node['cdms']['tomcat']['directory']['mode']}"
      action :create
    end
  end

  # url bind (drop war to tomcat webapp directory)
  execute "drop war to webapp directory" do
    cwd "#{Chef::Config[:file_cache_path]}"
    command "cp #{node['cdms']['tomcat']['file']} #{node['tomcat']['webapp_dir']}#{node['cdms']['tomcat']['root_uri']}.war"
  end

  # if there is something to change string, do it
  if !node['cdms']['tomcat']['str_before'].nil?
    execute 'sleep before deploying war file' do
      command "sleep 40"
    end

    execute "replace string #{node['cdms']['tomcat']['str_before']} to #{node['cdms']['tomcat']['str_after']}" do  
      command "sed -i 's/#{node['cdms']['tomcat']['str_before']}/#{node['cdms']['tomcat']['str_after']}/g' #{node['tomcat']['webapp_dir']}/#{node['cdms']['tomcat']['replace_file_path']}"
      user "#{node['tomcat']['user']}"
      group "#{node['tomcat']['group']}"
      not_if "grep #{node['cdms']['tomcat']['str_after']} #{node['tomcat']['webapp_dir']}/#{node['cdms']['tomcat']['replace_file_path']}"
    end
  end

end
# restart, enable tomcat6
service "tomcat6" do
  action [:enable, :restart]
end
