name             'cdms'
maintainer       'Cloud Deploy Managemnet System in Software Maestro 4th'
maintainer_email 'cdms@bitbucket.org'
license          'All rights reserved'
description      'Installs/Configures cdms'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'

recipe 'cdms::mysql', 'mysql-initial setting'
recipe 'cdms::war-deploy', 'war-deploy recipe to tomcat'
recipe 'cdms::jar-deploy', '*.jar(in ftp server) deploy on tomcat common library path'
recipe 'cdms::host', 'set the instance hostname in /etc/hosts'

depends 'database', '>= 2.2.1'
