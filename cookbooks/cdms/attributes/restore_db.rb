#
# Cookbook Name:: cdms
# Attributes:: restore_db 
#
# Copyright 2010, Opscode, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#default['cdms']['database']['user'] = nil 
#default['cdms']['database']['name'] = nil
#default['cdms']['database']['password'] = nil

# ftp_url : ftp://61.43.139.25/
# file : teletalkvi.sql -> ['cdms']['database']['password'].sql
default['cdms']['restore_db']['ftp_url'] = 'ftp://61.43.139.25/'
#default['cdms']['restore_db']['file'] = nil
