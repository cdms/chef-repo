#
# Cookbook Name:: cdms
# Attributes:: war-deploy
#
# Copyright 2010, Opscode, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#default['cdms']['tomcat']['ftp-url'] = 'ftp://61.43.139.25/'
#default['cdms']['tomcat']['file'] = 'jira-6.2-complete.war'
#default['cdms']['tomcat']['name'] = 'jira'
#default['cdms']['tomcat']['root-uri'] = '/jira'

default['cdms']['tomcat']['ftp_url'] = nil 
default['cdms']['tomcat']['file'] = nil
default['cdms']['tomcat']['name'] = nil
default['cdms']['tomcat']['root-uri'] = nil

default['cdms']['tomcat']['directory']['group'] =  nil
default['cdms']['tomcat']['directory']['mode'] = '00755'
default['cdms']['tomcat']['directory']['owner'] = nil
#default['cdms']['tomcat']['directory']['path'] = '/opt/jira_home'
default['cdms']['tomcat']['directory']['path'] = nil

default['cdms']['tomcat']['str_before'] = nil
default['cdms']['tomcat']['str_after'] = nil
default['cdms']['tomcat']['replace_file_path'] = nil
