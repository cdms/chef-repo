#
# Cookbook Name:: cdms
# Attributes:: jar-deploy
#
# Copyright 2010, Opscode, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

default['cdms']['tomcat']['jar_ftp_url'] = 'ftp://61.43.139.25/jar/'
default['cdms']['tomcat']['jar_list'] = []
default['cdms']['tomcat']['jar_mode'] = '00777'
#default['cdms']['tomcat']['jar_list'] = ['carol-1.5.2.jar','jonas_timer-1.4.3.jar','jta-1.0.1B.jar','ots-jts-1.0.jar','carol-properties-1.5.2.jar','jotm-1.4.3.jar','jul-to-slf4j-1.6.4.jar','slf4j-api-1.6.4.jar','hsqldb-1.8.0.5.jar','jotm-iiop_stubs-1.4.3.jar','log4j-1.2.16.jar','slf4j-log4j12-1.6.4.jar','jcl-over-slf4j-1.6.4.jar','jotm-jrmp_stubs-1.4.3.jar','objectweb-datasource-1.4.3.jar','xapool-1.3.1.jar']

