#
# Cookbook Name:: war-tomcat
# Recipe:: default
#
# Copyright 2014, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

# todo : setup environment for this cookbook (ex. unzip, ...)


# todo : stop tomcat service
service "tomcat6" do
  action :stop
end

# for each war to deploy
node['WAS']['apps'].each do |app|
  # todo : copy common libs to proper location.
  #remote_file "download common lib .zip file." do action end

  # copy *.war files to chef Cache dirtory.
  remote_file "start deploy #{app['name']}'s .war file" do
    path "#{Chef::Config[:file_cache_path]}/#{app['file_path']}"
    source "#{app['ftp_url']}#{app['file_path']}"
  end

  # create directory that is needed by war app.
  app['directories'].each do |dir|
    directory "#{dir['path']}" do
      owner "#{node['tomcat']['user']}"
      mode "#{dir['mode']}"
      action :create
    end
  end

  # database shell script run
  app['databases'].each do |db|

    remote_file "#{Chef::Config[:file_cache_path]}/#{app['name']}-#{db['type']}.erb" do
      source "#{db['config']}"
      mode "0775" 
    end

    template "#{Chef::Config[:file_cache_path]}/#{app['name']}-#{db['type']}.sh" do
      local true
      source "#{Chef::Config[:file_cache_path]}/#{app['name']}-#{db['type']}.erb"
      variables :variable => {  
        "user" => "root", # not use
        "passwd" => "#{node['mysql']['server_root_password']}",
        "host" => 'localhost' # fixme
      }
    end

    execute "run shell script for db" do 
      cwd Chef::Config[:file_cache_path]
      command "bash #{app['name']}-#{db['type']}.sh"
    end
  end
 
  # url bind
  execute "drop war to webapp dir" do
    cwd "#{Chef::Config[:file_cache_path]}"
    command "cp #{app['file_path']} #{node['tomcat']['webapp_dir']}#{app['root_uri']}.war"
  end

end

# todo : restart, enable tomcat service
service "tomcat6" do
  action [:enable, :restart]
end
