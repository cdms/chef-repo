#package "glusterfs-fuse" do
#	action :install
#end

package "glusterfs-client" do
  action :upgrade
end

execute "add-fuse-to-modules" do
  command "echo fuse >> /etc/modules"
  user "root"
  creates "/var/tmp/.add-fuse-to-modules"
end

file "/var/tmp/.add-fuse-to-modules"

mount_to=node['glusterfs']['client']['mount']
if not File.directory?(mount_to)
  directory mount_to do
    action :create
    recursive true
  end
end

bag_name = node['glusterfs']['server']['databag']
gluster_config = data_bag_item(bag_name, 'config')

server = gluster_config['init_node']

volume = node['glusterfs']['server']['volume']
# mount -t glusterfs -o log-level=WARNING,log-file=/var/log/gluster.log 10.200.1.11:/test /mnt
mount mount_to do
  device "#{server}:/#{volume}"
  fstype "glusterfs"
  options "log-level=WARNING,log-file=/var/log/gluster.log"
end

#node['glusterfs']['client']['mount'].each do |volume, mount_to|
#	if not File.directory?(mount_to)
#		directory mount_to do
#		action :create
#		recursive true
#	  end
#	end
#
#	# mount -t glusterfs -o log-level=WARNING,log-file=/var/log/gluster.log 10.200.1.11:/test /mnt
#	#server =  node['glusterfs']['server']['peers'][0]
#	gluster_config = data_bag_item(node['glusterfs']['server']['databag'], 'config')
#	server = gluster_config['init_node']
#	mount mount_to do
#		device "#{server}:/#{volume}"
#		fstype "glusterfs"
#		options "log-level=WARNING,log-file=/var/log/gluster.log"
#	end
#end
