default['glusterfs']['server']['export_directory'] = "/export/glusterfs"

default['glusterfs']['server']['peers'] = [
	"10.0.0.3",
	"10.0.0.4",
	"10.0.0.5",
	"10.0.0.6",
	"10.0.0.7"

#	"61.43.139.235",
#	"61.43.139.236"
]

#default['glusterfs]['server]['volumes] = ["test"]
#default['glusterfs']['server']['volumes'] = ["cinder-volume"]
#default['glusterfs']['server']['volume'] = "cinder-volume"
default['glusterfs']['server']['volume'] = nil

#default['glusterfs']['server']['databag'] = "gluster"
default['glusterfs']['server']['databag'] = nil
